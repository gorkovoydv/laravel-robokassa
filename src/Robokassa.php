<?php

namespace Chelout\Robokassa;

use Lexty\Robokassa\Auth;
use Lexty\Robokassa\Payment;

class Robokassa
{
    protected $testMode;
    protected $shopId;
    protected $hashAlgo;
    protected $paymentPassword;
    protected $validationPassword;
    protected $resultUrl;
    protected $successUrl;
    protected $failUrl;

    protected $auth;
    protected $payment;

    public function __construct(array $config)
    {
        if (empty($config)) {
            throw new \Exception('Config is not defined');
        }

        $this->testMode = $config['test_mode'];
        $this->shopId = $config['shop_id'];
        $this->hashAlgo = $config['hash_algorithm'];
        $this->paymentPassword = $config['payment_password'];
        $this->validationPassword = $config['validation_password'];
        $this->resultUrl = $config['result_url'];
        $this->successUrl = $config['success_url'];
        $this->failUrl = $config['fail_url'];

        $this->auth = new Auth($this->shopId, $this->paymentPassword, $this->validationPassword, $this->testMode);
        $this->auth->setHashAlgo($this->hashAlgo);
    
        $this->payment = new Payment($this->auth);
    }

    /**
     * @return Payment
     */
    public function getPayment(): Payment
    {
        return $this->payment;
    }

    /**
     * @return Auth
     */
    public function getAuth(): Auth
    {
        return $this->auth;
    }

}
